const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/library.js', 'public/js')
    .js('resources/js/forms/login.js', 'public/js/forms')
    .js('resources/js/forms/register.js', 'public/js/forms')
    .sass('resources/sass/app.scss', 'public/css');
