/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/forms/register.js":
/*!****************************************!*\
  !*** ./resources/js/forms/register.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function (exports) {
  function valOrFunction(val, ctx, args) {
    if (typeof val == "function") {
      return val.apply(ctx, args);
    } else {
      return val;
    }
  }

  function InvalidInputHelper(input, options) {
    input.setCustomValidity(valOrFunction(options.defaultText, window, [input]));

    function changeOrInput() {
      if (input.value == "") {
        input.setCustomValidity(valOrFunction(options.emptyText, window, [input]));
      } else {
        input.setCustomValidity("");
      }
    }

    function invalid() {
      if (input.value == "") {
        input.setCustomValidity(valOrFunction(options.emptyText, window, [input]));
      } else {
        input.setCustomValidity(valOrFunction(options.invalidText, window, [input]));
      }
    }

    input.addEventListener("change", changeOrInput);
    input.addEventListener("input", changeOrInput);
    input.addEventListener("invalid", invalid);
  }

  exports.InvalidInputHelper = InvalidInputHelper;
})(window);

InvalidInputHelper(document.getElementById("input-username"), {
  defaultText: "Please enter an username!",
  emptyText: "Please enter an username!",
  invalidText: function invalidText(input) {
    return 'The username "' + input.value + '" is invalid!';
  }
});
InvalidInputHelper(document.getElementById("input-email"), {
  defaultText: "Please enter an email address!",
  emptyText: "Please enter an email address!",
  invalidText: function invalidText(input) {
    var result = 'The email "' + input.value + '" is invalid! Please use "@" to input your email';
    document.getElementById("input-email").value = "";
    return result;
  }
});
InvalidInputHelper(document.getElementById("input-password"), {
  defaultText: "Please enter a password!",
  emptyText: "Please enter a password!",
  invalidText: function invalidText(input) {
    return 'The password "' + input.value + '" is invalid!';
  }
});
InvalidInputHelper(document.getElementById("input-repassword"), {
  defaultText: "Please enter a re-password!",
  emptyText: "Please enter a re-password!",
  invalidText: function invalidText(input) {
    return 'The password "' + input.value + '" is invalid!';
  }
});

window.matchPassword = function () {
  if (document.getElementById('input-password').value == document.getElementById('input-repassword').value) {
    document.getElementById('submit-button').disabled = false;
    document.getElementById('match-password-message').classList.remove('text-danger');
    document.getElementById('match-password-message').classList.add('text-primary');
    document.getElementById('match-password-message').innerHTML = "match";
  } else {
    document.getElementById('submit-button').disabled = true;
    document.getElementById('match-password-message').classList.remove('text-primary');
    document.getElementById('match-password-message').classList.add('text-danger');
    document.getElementById('match-password-message').innerHTML = "not match";
  }
};

/***/ }),

/***/ 3:
/*!**********************************************!*\
  !*** multi ./resources/js/forms/register.js ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/dailysocial/Public/laravel/mercury/resources/js/forms/register.js */"./resources/js/forms/register.js");


/***/ })

/******/ });