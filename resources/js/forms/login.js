(function (exports) {
    function valOrFunction(val, ctx, args) {
        if (typeof val == "function") {
            return val.apply(ctx, args);
        } else {
            return val;
        }
    }

    function InvalidInputHelper(input, options) {
        input.setCustomValidity(valOrFunction(options.defaultText, window, [input]));

        function changeOrInput() {
            if (input.value == "") {
                input.setCustomValidity(valOrFunction(options.emptyText, window, [input]));
            } else {
                input.setCustomValidity("");
            }
        }

        function invalid() {
            if (input.value == "") {
                input.setCustomValidity(valOrFunction(options.emptyText, window, [input]));
            } else {
               input.setCustomValidity(valOrFunction(options.invalidText, window, [input]));
            }
        }

        input.addEventListener("change", changeOrInput);
        input.addEventListener("input", changeOrInput);
        input.addEventListener("invalid", invalid);
    }
    exports.InvalidInputHelper = InvalidInputHelper;
})(window);

InvalidInputHelper(document.getElementById("input-username"), {
    defaultText: "Please enter an email / username!",
    emptyText: "Please enter an email /username!",
    invalidText: function (input) {
        return 'The email address "' + input.value + '" is invalid!';
    }
});

InvalidInputHelper(document.getElementById("input-password"), {
    defaultText: "Please enter a correct password!",
    emptyText: "Please enter a correct password!",
    invalidText: function (input) {
        return 'The email address "' + input.value + '" is invalid!';
    }
});