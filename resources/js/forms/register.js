(function (exports) {
    function valOrFunction(val, ctx, args) {
        if (typeof val == "function") {
            return val.apply(ctx, args);
        } else {
            return val;
        }
    }

    function InvalidInputHelper(input, options) {
        input.setCustomValidity(valOrFunction(options.defaultText, window, [input]));

        function changeOrInput() {
            if (input.value == "") {
                input.setCustomValidity(valOrFunction(options.emptyText, window, [input]));
            } else {
                input.setCustomValidity("");
            }
        }

        function invalid() {
            if (input.value == "") {
                input.setCustomValidity(valOrFunction(options.emptyText, window, [input]));
            } else {
               input.setCustomValidity(valOrFunction(options.invalidText, window, [input]));
            }
        }

        input.addEventListener("change", changeOrInput);
        input.addEventListener("input", changeOrInput);
        input.addEventListener("invalid", invalid);
    }
    exports.InvalidInputHelper = InvalidInputHelper;
})(window);

InvalidInputHelper(document.getElementById("input-username"), {
    defaultText: "Please enter an username!",
    emptyText: "Please enter an username!",
    invalidText: function (input) {
        return 'The username "' + input.value + '" is invalid!';
    }
});

InvalidInputHelper(document.getElementById("input-email"), {
    defaultText: "Please enter an email address!",
    emptyText: "Please enter an email address!",
    invalidText: function (input) {
        var result =  'The email "' + input.value + '" is invalid! Please use "@" to input your email';
        document.getElementById("input-email").value = "";
        return result;
    }
});

InvalidInputHelper(document.getElementById("input-password"), {
    defaultText: "Please enter a password!",
    emptyText: "Please enter a password!",
    invalidText: function (input) {
        return 'The password "' + input.value + '" is invalid!';
    }
});

InvalidInputHelper(document.getElementById("input-repassword"), {
    defaultText: "Please enter a re-password!",
    emptyText: "Please enter a re-password!",
    invalidText: function (input) {
        return 'The password "' + input.value + '" is invalid!';
    }
});

window.matchPassword = function() {
    if(document.getElementById('input-password').value == document.getElementById('input-repassword').value) {
        document.getElementById('submit-button').disabled = false;
        document.getElementById('match-password-message').classList.remove('text-danger');
        document.getElementById('match-password-message').classList.add('text-primary');
        document.getElementById('match-password-message').innerHTML = "match";
    }else{
        document.getElementById('submit-button').disabled = true;
        document.getElementById('match-password-message').classList.remove('text-primary');
        document.getElementById('match-password-message').classList.add('text-danger');
        document.getElementById('match-password-message').innerHTML = "not match";
    }
}