<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>@yield('title')Incubation</title>

    <link rel="stylesheet" type="text/css" href="css/app.css" />
    @yield('stylesheets')

    @yield('topscript')
</head>

<body>

    <main>
        @yield('content')
    </main>
    
    
    <script type="text/javascript" src="js/app.js"></script>
    @yield('bottomscript')
</body>

</html>