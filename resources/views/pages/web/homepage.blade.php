@extends('base')
@section('title', 'Homepage | ')

@section('content')
    <div class="mobile-view mobile-view--shadow pt-5 pb-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-9">
                    <div class="banner__image-wrapper mb-5">
                        <div class="banner__image-cover">
                            <div class="banner__image-show banner__image--banner"></div>
                        </div>
                    </div>
                    <div class="content__text-wrapper content__text-wrapper--center mb-5">
                        <img class="logo-image mb-3" src="{{ asset('images/orchestra-logo.png') }}" />
                        <h1 class="text__title font-weight-bold">Welcome to Family</h1>
                        <p class="text__description">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard.
                        </p>
                    </div>
                    <div class="content__btn-wrapper">
                        <div class="mb-3 text-center">
                            <button class="button button--rounded button--logo-color w-100 text-white font-weight-bold pt-2 pb-2" onclick="window.location.href='/register'">Register</button>
                        </div>
                        <div class="mb-3 text-center">
                            <button class="button button--rounded button--white-color w-100 font-weight-bold pt-2 pb-2" onclick="window.location.href='/login'">Log In</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection