@extends('base')
@section('title', 'Login | ')

@section('content')
<div class="mobile-view mobile-view--shadow pt-5 pb-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-9">
                <div class="banner__image-wrapper mb-5">
                    <div class="banner__image-cover">
                        <div class="banner__image-show banner__image--register-success"></div>
                    </div>
                </div>
                <div class="content__text-wrapper content__text-wrapper--center mb-5">
                    <h1 class="text__title font-weight-bold">Registration Success!</h1>
                    <p class="text__description">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
                    </p>
                </div>
                <div class="content__btn-wrapper">
                    <div class="mb-3 text-center">
                        <button class="button button--rounded button--logo-color w-100 text-white font-weight-bold pt-2 pb-2">Complete Profile</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('bottomscript')
    <script type="text/javascript" src="js/library.js"></script>
    <script type="text/javascript" src="js/forms/login.js"></script>
@endsection