@extends('base')
@section('title', 'Register | ')

@section('content')
    <div class="mobile-view mobile-view--shadow pt-5 pb-5">
        @include('partials/back-button')
        <div class="container mobile-view--height">
            <div class="row justify-content-center align-items-center mobile-view--height">
                <div class="col-9">
                    <div class="content__text-wrapper content__text-wrapper--center mb-5">
                        <h1 class="text__title font-weight-bold mb-4">Registration</h1>
                        <p class="text__description">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's.
                        </p>
                    </div>
                    <div class="content__text-wrapper mb-5">
                        <form>
                            <div class="form-group-2">
                                <input id="input-username" type="text" class="form-control-2" name="username" required="required"/>
                                <span class="highlight"></span><span class="form-control-2__bar"></span>
                                <label class="form-control-2__label">Username</label>
                            </div>
                            <div class="form-group-2">
                                <input id="input-email" type="email" class="form-control-2" name="email" required="required"/>
                                <span class="highlight"></span><span class="form-control-2__bar"></span>
                                <label class="form-control-2__label">Email</label>
                            </div>
                            <div class="form-group-2">
                                <input id="input-password" type="password" class="form-control-2 same-password" name="password" required="required"/>
                                <span class="highlight"></span><span class="form-control-2__bar"></span>
                                <label class="form-control-2__label">Password</label>
                            </div>
                            <div class="form-group-2">
                                <input id="input-repassword" type="password" class="form-control-2 same-password" required="required" onkeyup='matchPassword()'; />
                                <span class="highlight"></span><span class="form-control-2__bar"></span>
                                <label class="form-control-2__label">Re-type Password</label>
                            </div>
                            <div>
                                <span id="match-password-message" class="text__description"></span>
                            </div>
                            <div class="pt-5">
                                <button id="submit-button" class="button button--rounded button--logo-color w-100 text-white font-weight-bold pt-2 pb-2">Register</button>
                            </div>
                        </form>
                    </div>
                    <div class="content__text-wrapper content__text-wrapper--center">
                        <a class="text__forgot-password" href="">Do you have an account? Login now!</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('bottomscript')
    <script type="text/javascript" src="js/library.js"></script>
    <script type="text/javascript" src="js/forms/register.js"></script>
@endsection